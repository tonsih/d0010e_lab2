package lab2;

import lab2.level.Level;
import lab2.level.LevelGUI;
import lab2.level.Room;
import java.awt.Color;
import java.util.ArrayList;

public class Driver {
	
	// ArrayList containing all of the levels. A GUI will be created for these.
	ArrayList<Level> levels = new ArrayList<Level>();

	public void run() {
		

		// -------------------------------------
		//     		 --- LEVELS ---
		// ------------------------------------
		
		
		// -------- Level 1 --------

		// Creates the rooms with their coordinates (x,y,color)
		Room r1_levelOne = new Room(100,200,Color.BLUE);
		Room r2_levelOne = new Room(100,200,Color.RED);
		Room r3_levelOne = new Room(100,150,Color.GREEN);
		Room r4_levelOne = new Room(150,150,Color.decode("#808080"));
		Room r5_levelOne = new Room(150,150,Color.decode("#e3256b"));

		
		// Connects the created rooms with each other
		r1_levelOne.connectSouthTo(r2_levelOne);
		
		r2_levelOne.connectNorthTo(r1_levelOne);
		r2_levelOne.connectSouthTo(r3_levelOne);
		
		r3_levelOne.connectNorthTo(r2_levelOne);
		r3_levelOne.connectEastTo(r4_levelOne);
		
		r4_levelOne.connectWestTo(r3_levelOne);
		r4_levelOne.connectEastTo(r5_levelOne);
		
		r5_levelOne.connectWestTo(r4_levelOne);
		r5_levelOne.connectEastTo(r1_levelOne);
		
		
		// Creates the level
		Level levelOne = new Level(); 
	
		
		//Places the levels. Is given the x,y coordinates (room,x,y)
		levelOne.place(r1_levelOne,100,200);
		levelOne.place(r2_levelOne,100,450);
		levelOne.place(r3_levelOne,100,700);
		levelOne.place(r4_levelOne,300,700);
		levelOne.place(r5_levelOne,550,700);

		
		// Assigns the room where the player starts
		levelOne.firstLocation(r1_levelOne);
		
		// Adds the level to the ArrayList "levels".
		levels.add(levelOne);
		

		// -------- Level 2 --------

		// Creates the rooms with their coordinates (x,y,color)
		Room r1_levelTwo = new Room(300,100,Color.decode("#413620"));
		Room r2_levelTwo = new Room(250,200,Color.decode("#57A773"));
		Room r3_levelTwo = new Room(100,150,Color.decode("#987284"));
		Room r4_levelTwo = new Room(150,150,Color.decode("#FF9505"));
		Room r5_levelTwo = new Room(150,150,Color.decode("#78C3FB"));
		Room r6_levelTwo = new Room(150,150,Color.decode("#2B193D"));

		
		// Connects the created rooms with each other
		r1_levelTwo.connectEastTo(r2_levelTwo);
		r1_levelTwo.connectSouthTo(r4_levelTwo);

		r2_levelTwo.connectSouthTo(r3_levelTwo);
		
		r3_levelTwo.connectNorthTo(r2_levelTwo);
		r3_levelTwo.connectWestTo(r4_levelTwo);
        
		r4_levelTwo.connectEastTo(r3_levelTwo);
		r4_levelTwo.connectNorthTo(r1_levelTwo);
		r4_levelTwo.connectSouthTo(r5_levelTwo);
		
		r5_levelTwo.connectWestTo(r1_levelTwo);
		r5_levelTwo.connectEastTo(r6_levelTwo);
		
		r6_levelTwo.connectEastTo(r1_levelTwo);
		

		// Creates the level
		Level levelTwo = new Level(); 
	

		// Places the levels. Is given the x,y coordinates (room,x,y)
		levelTwo.place(r1_levelTwo,100,250);
		levelTwo.place(r2_levelTwo,500,200);
		levelTwo.place(r3_levelTwo,500,500);
		levelTwo.place(r4_levelTwo,250,500);
		levelTwo.place(r5_levelTwo,250,750);
		levelTwo.place(r6_levelTwo,500,750);

		
		// Assigns the room where the player starts
		levelTwo.firstLocation(r3_levelTwo);
		
		
		// Adds the level to the ArrayList "levels".
		levels.add(levelTwo);


		// -------- Level 3 --------

		// Creates the rooms with their coordinates (x,y,color)
		Room r1_levelThree = new Room(50,300,Color.decode("#5758BB"));
		Room r2_levelThree = new Room(100,200,Color.decode("#EAB543"));
		Room r3_levelThree = new Room(100,150,Color.decode("#2f3542"));
		Room r4_levelThree = new Room(150,150,Color.decode("#1289A7"));
		Room r5_levelThree = new Room(150,150,Color.decode("#82589F"));

		
		// Connects the created rooms with each other
		r1_levelThree.connectEastTo(r2_levelThree);
		r1_levelThree.connectWestTo(r4_levelThree);

		r2_levelThree.connectWestTo(r1_levelThree);
		r2_levelThree.connectEastTo(r4_levelThree);
		r2_levelThree.connectSouthTo(r3_levelThree);
		r2_levelThree.connectNorthTo(r5_levelThree);

		r3_levelThree.connectNorthTo(r2_levelThree);
		r3_levelThree.connectSouthTo(r4_levelThree);
		r3_levelThree.connectEastTo(r5_levelThree);
		
		r4_levelThree.connectWestTo(r2_levelThree);
		r4_levelThree.connectEastTo(r1_levelThree);
		r4_levelThree.connectSouthTo(r5_levelThree);
		
		
		r5_levelThree.connectNorthTo(r4_levelThree);
		r5_levelThree.connectEastTo(r1_levelThree);
		r5_levelThree.connectWestTo(r3_levelThree);
		
		// Creates the level
		Level levelThree = new Level(); 
	
		// Places the levels. Is given the x,y coordinates (room,x,y)
		levelThree.place(r1_levelThree,100,200);
		levelThree.place(r2_levelThree,300,150);
		levelThree.place(r3_levelThree,300,450);
		levelThree.place(r4_levelThree,575,300);
		levelThree.place(r5_levelThree,570,500);

		// Assigns the room where the player starts
		levelThree.firstLocation(r5_levelThree);
		
		// Adds the level to the ArrayList "levels".
		levels.add(levelThree);
		
		// -------------------------------------
		//     	   --- END OF LEVELS ---
		// ------------------------------------


		// Creates a GUI for the level. Select level with "levels.get(x)"
		// where x is 0 to 2.
		 new LevelGUI(levels.get(1), "GUI");
	}

}
