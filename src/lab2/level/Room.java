package lab2.level;

import java.awt.Color;

public class Room {
	
	// Initialized dynamic variables for the room's connections.
	Room northWall = null; 
	Room eastWall = null; 
	Room southWall = null; 
	Room westWall = null; 
	
	// Dynamic variable for the color of the room object.
	Color floorColor;
	
	// Dynamic variables for the rooms coordinates.
	int x;
	int y;


	// Dynamic variables for the rooms width and height.
	int dx;
	int dy;
	
	
	// Constructor for Room, takes the given width=dx, height=dy and color
	// as parameters
	public Room(int dx, int dy, Color color) {
		this.dx = dx;
		this.dy = dy;

		floorColor = color;
//		System.out.println(dx + " " + dy + " " + floorColor);
	}
	

	// Takes a room as a parameter and connects it with another room 
	// depending on the chosen direction
	public void connectNorthTo(Room r) {
		northWall = r;
	}

	public void connectEastTo(Room r) {
		eastWall = r;
	}

	public void connectSouthTo(Room r) {
		southWall = r;
	}

	public void connectWestTo(Room r) {
		westWall = r;
	}
	
}
