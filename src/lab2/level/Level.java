package lab2.level;

import java.util.ArrayList;
import java.util.Observable;

public class Level extends Observable {
	Room currentRoom;
	ArrayList<Room> rooms = new ArrayList<Room>();

	// In some scenarios where the placed rooms overlap, drawing the player
	// character will cause errors. This is avoided by not drawing the character.
	boolean drawGubbe = true;

	private boolean firstLocationExists = false;

	// Checks for cases of overlap. Overlap detected if even one conditions is
	// true.
	private boolean overlapCheck(int nx, int ny, int ndx, int ndy, int jx, 
			int jy, int jdx, int jdy) {
		if ((jx + jdx < nx || jx > nx + ndx) || 
		    (jy + jdy < ny || jy > ny + ndy)) {
			return true;
		} else {
			return false;
		}
	}

	// Method for placing the rooms. Takes the room object and the coordinates
	// in x and y directions. Returns true if there's no overlap, otherwise
	// returns false.
	public boolean place(Room j, int x, int y) {

		if (firstLocationExists) {
			System.out.println("Can't place any more rooms. " + 
		"First location already initialized!");
			return false;
		}

		// Loops through all rooms in ArrayList rooms. Returns 
		for (Room n : rooms) {
			if (!overlapCheck(n.x, n.y, n.dx, n.dy, x, y, j.dx, j.dy)) {
				drawGubbe = false;
				return false;
			}
			;
		}

		// If no overlap, then the given room will be placed by assigning it
		// the given coordinates.
		j.x = x;
		j.y = y;

		// Adds the room with it's updated coordinated to the "rooms" ArrayList
		rooms.add(j);

		return true;

	}

	// Decides where the player is drawn, when the program first runs
	public void firstLocation(Room r) {
		if (rooms.contains(r)) {
			firstLocationExists = true;
			currentRoom = r;

		}

	}

	// Movement methods. Checks if player can be moved to location.
	// assigns the player or "moves" the player to that location if it can be.

	void moveNorth() {
		if (currentRoom.northWall != null) {
			currentRoom = currentRoom.northWall;
			setChanged();
			notifyObservers();
		} else {
			System.out.println("No wall to the north!");
		}

	}

	void moveSouth() {
		if (currentRoom.southWall != null) {
			currentRoom = currentRoom.southWall;
			setChanged();
			notifyObservers();
		} else {
			System.out.println("No wall to the south!");
		}


	}

	void moveEast() {
		if (currentRoom.eastWall != null) {
			currentRoom = currentRoom.eastWall;
			setChanged();
			notifyObservers();
		} else {
			System.out.println("No wall to the east!");
		}

	}

	void moveWest() {
		if (currentRoom.westWall != null) {
			currentRoom = currentRoom.westWall;
			setChanged();
			notifyObservers();
		} else {
			System.out.println("No wall to the west!");
		}


	}
}
