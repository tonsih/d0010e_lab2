package lab2.level;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class LevelGUI implements Observer {
	private Level lv;
	private Display d;
	
	public LevelGUI(Level level, String name) {
		
		this.lv = level;
		
		JFrame frame = new JFrame(name);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Resolution of the level (600x600)
		d = new Display(lv,600,600);
		
		frame.getContentPane().add(d);
		frame.pack();
		frame.setLocation(0,0);
		frame.setVisible(true);
		
		// Makes the LevelGUI object the observer of the 
		// level instance (this.lv) 
		this.lv.addObserver(this);
	}
	
	// Activated when SetChanged() and NotifyObservers are called in the
	// observable level-object (See Level-class).
	// Repaints the levelGUI with d.repaint().
	public void update(Observable arg0, Object arg1) {
	 d.repaint();
	}
	
	private class Display extends JPanel {
		
		public Display(Level fp, int x, int y) {
		
			addKeyListener(new Listener());
			
			setBackground(Color.BLACK);
			setPreferredSize(new Dimension(x+20,y+20));
			setFocusable(true);
		}
		
				
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			drawRooms(lv.rooms, g);
			drawDoors(lv.rooms, g);
			
			if (lv.drawGubbe) {
				drawCharacter(lv.currentRoom, g);
			}
		}
		
		
		private void drawRooms(ArrayList<Room> rooms, Graphics g) {
			for(Room r : rooms) {
				g.setColor(r.floorColor);
				g.fillRect(r.x, r.y, r.dx, r.dy);
			}
		}
		
		// Doors/walls graphics.
		// Loops through the rooms in the "rooms"-ArrayList and checks,
		// which room's are connected with the other rooms.
		private void drawDoors(ArrayList<Room> rooms, Graphics g) {
			for(Room r : rooms) {
				if (r.northWall != null) {
					g.setColor(r.northWall.floorColor);
	     			g.fillRect(r.x+r.dx/4, r.y, r.dx/2, 10);
				}
				
				if (r.southWall != null) { 
					g.setColor(r.southWall.floorColor); 
	     			g.fillRect(r.x+r.dx/4, r.y+r.dy-10, r.dx/2, 10);
				} 
				
				if (r.eastWall != null) {
					g.setColor(r.eastWall.floorColor);
	     			g.fillRect(r.x+r.dx-10, r.y+r.dy/4, 10, r.dy/2);
				}
				
				if (r.westWall != null) {
					g.setColor(r.westWall.floorColor);
	     			g.fillRect(r.x, r.y+r.dy/4, 10, r.dy/2);
				}
			}
		}
		
		//Character graphics (size, shape and color)
		private void drawCharacter(Room r, Graphics g) {
			int characterSize = 30;
			
			g.setColor(new Color(255,255,255));
			g.fillOval((r.x+r.dx/2)-characterSize/2, 
			(r.y+r.dy/2)-characterSize/2, 30, 30);
		}
		
		

	 	private class Listener implements KeyListener {

	 		
	 		public void keyPressed(KeyEvent arg0) {
	 		}

	 		public void keyReleased(KeyEvent arg0) {
	 		}

	 		public void keyTyped(KeyEvent event) {
	 			// Checks for key typed (pressed or released won't matter)
	 			// Movement with the WASD keys.
	 			switch (event.getKeyChar()) {
	 	         case 'w':
	 	           lv.moveNorth();
	 	           break;
	 	         case 'd':
	 	           lv.moveEast();
	 	           break;
	 	         case 's':
	 	           lv.moveSouth();
	 	           break;
	 	         case 'a':
	 	           lv.moveWest();
	 	           break;
	 	         default:
	 	           System.out.println("You can only move with the keys: "
	 	           		+ "W, S, A " + "or D.");
	 	           break;
	 	         }
	 			
	 		}
	 	}

	}
	
}
